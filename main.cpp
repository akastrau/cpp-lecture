
//Przyklad 1
//#include <iostream>
//#include <thread>
//
//void callable(int id){
//    std::cout << "Hello from thread " << id << std::endl;
//}
//
//int main() {
//    std::thread *thread = new std::thread(&callable, 1);
//    thread->join();
//    return 0;
//}
//Przyklad 2

//#include <iostream>
//#include <future>
//
//int calculateSum(int x, int y){ //przedzial <x;y>
//    int sum = 0;
//
//    for (int i = x; i < y + 1; ++i) {
//        sum += i;
//    }
//    return sum;
//}
//
//int main(){
//    auto value1 = std::async(&calculateSum, 1, 3);
//    auto value2 = std::async(&calculateSum, 4, 6);
//
//    auto future1 = value1.get();
//    auto future2 = value2.get();
//
//    std::cout << "Sum: " << future1 + future2 << std::endl;
//    return EXIT_SUCCESS;
//}

//Przyklad 3
//#include <iostream>
//#include <future>
//
//int calculateSum(int x, int y){ //przedzial <x;y>
//    int sum = 0;
//    std::cout << "Thread " << std::this_thread::get_id() << " started!" << std::endl << std::flush;
//
//    for (int i = x; i < y + 1; ++i) {
//        std::this_thread::sleep_for(std::chrono::seconds(rand() % 10));
//        sum += i;
//    }
//    std::cout << "Thread " << std::this_thread::get_id() << " exiting..." << std::endl << std::flush;
//    return sum;
//}
//
//int main(){
//    srand((unsigned)time(NULL));
//    std::cout << "We're starting..." << std::endl << std::flush;
//    auto value1 = std::async(std::launch::async, &calculateSum, 1, 3);
//    std::cout << "After value 1" << std::endl << std::flush;
//    auto value2 = std::async(std::launch::async, &calculateSum, 4, 6);
//    std::cout << "After value 2" << std::endl << std::flush;
//
//    auto future1 = value1.get();
//    auto future2 = value2.get();
//
//    std::cout << "Sum: " << future1 + future2 << std::endl;
//    return EXIT_SUCCESS;
//}

//Przyklad 4

//#include <iostream>
//#include <thread>
//
//class Computation {
//public:
//    void calculateSum(int x, int y){
//        int sum = 0;
//        for (int i = x; i < y + 1; ++i) {
//            sum += i;
//        }
//        std::cout << "Sum: " << sum << std::endl << std::flush;
//    }
//};
//
//int main(){
//    std::thread thread(&Computation::calculateSum, Computation(), 1, 3);
//    thread.join();
//    return EXIT_SUCCESS;
//}

//Przyklad 5

//#include <thread>
//#include <iostream>
//
//class Computation {
//public:
//    void calculateSum(int x, int y, int & ref){
//        int sum = 0;
//        for (int i = x; i < y + 1; ++i) {
//            sum += i;
//        }
//        ref = sum;
//    };
//};
//
//int main(){
//    int sum = 0;
//    std::thread thread(&Computation::calculateSum, Computation(), 1, 3, std::ref(sum));
//    thread.join();
//    std::cout << "Sum: " << sum << std::endl;
//    return EXIT_SUCCESS;
//}

//Przyklad 6

//#include <iostream>
//#include <thread>
//#include <mutex>
//
//std::mutex mutex;
//void calculateSum(int & ref){
//    int sum = 0;
//    std::lock_guard<std::mutex> guard(mutex);
//    for (int i = 0; i < 1000; ++i) {
//        sum += i;
//    }
//    ref += sum;
//    //nie trzeba wywolywac metody unblock()
//}
//
//int main(){
//    int sum = 0;
//    std::thread thread1(&calculateSum, std::ref(sum));
//    std::thread thread2(&calculateSum, std::ref(sum));
//    thread1.join();
//    thread2.join();
//
//    std::cout << "Sum: " << sum << std::endl;
//
//    return EXIT_SUCCESS;
//}

//Przykład 7

#include <iostream>
#include <thread>
#include <atomic>

void calculateSum(std::atomic<int> & ref){
    int sum = 0;
    for (int i = 0; i < 1000; ++i) {
        sum += i;
    }
    ref += sum;
}

int main(){
    std::atomic<int> sum(0);
    std::thread thread(&calculateSum, ref(sum));
    std::thread thread2(&calculateSum, ref(sum));
    thread.join();
    thread2.join();

    std::cout << "Sum: " << sum << std::endl;
    return EXIT_SUCCESS;
}